class Calculator {
    static Double getDeterminant(Matrix matrix) {
        return matrix.getByDimensions(0, 0) * matrix.getByDimensions(1, 1) - matrix.getByDimensions(1, 0) * matrix.getByDimensions(0, 1);
    }
}
