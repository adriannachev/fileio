import java.util.ArrayList;
import java.util.List;

class Matrix {
    private Matrix(){}

    private List<List<Double>> matrixRepresentation = new ArrayList<>();

    static Matrix initializeMatrix(List<List<Double>> rows) {
        Matrix matrix = new Matrix();
        validate(rows);
        matrix.matrixRepresentation.addAll(rows);
        return matrix;
    }

    private static void validate(List<List<Double>> rows) {
        if (rows.size() != 2) {
            throw new IllegalArgumentException("The matrix must have exactly 2 rows");
        }
        if (rows.get(0).size() != 2 || rows.get(1).size() != 2) {
            throw new IllegalArgumentException("Each row of the matrix must be exactly 2 columns");
        }
    }

    double getDeterminant() {
        return Calculator.getDeterminant(this);
    }

    double getByDimensions(int x, int y) {
        return matrixRepresentation.get(x).get(y);
    }
}
