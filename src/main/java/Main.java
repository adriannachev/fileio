import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please supply an inputFile");
        String inputFile = scanner.nextLine();
        System.out.println("Please supply an outputFile");
        String outputFile = scanner.nextLine();

        MatrixFileManipulator matrixFileManipulator = MatrixFileManipulator.initFileManipulator();
        List<Matrix> matrices = matrixFileManipulator.getMatricesFromFile(inputFile);
        matrixFileManipulator.writeMatricesToFile(outputFile, matrices);
    }
}
