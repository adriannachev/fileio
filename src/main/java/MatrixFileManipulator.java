import java.io.*;
import java.util.ArrayList;
import java.util.List;

class MatrixFileManipulator {
    private List<List<Double>> matrixRows = new ArrayList<>();
    private List<Double> matrixRow = new ArrayList<>();

    private MatrixFileManipulator() {
    }

    static MatrixFileManipulator initFileManipulator() {
        return new MatrixFileManipulator();
    }

    List<Matrix> getMatricesFromFile(String inputFile) {
        List<Matrix> matrices = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {

            String line = reader.readLine();

            while (line != null) {
                if (line.isEmpty()) {
                    Matrix matrix = Matrix.initializeMatrix(matrixRows);
                    matrices.add(matrix);
                    emptyMatrixRows();
                } else {
                    populateMatrixRowFromLine(line);
                    matrixRows.add(matrixRow);
                    emptyCurrentRow();
                }
                line = reader.readLine();
            }
            Matrix matrix = Matrix.initializeMatrix(matrixRows);
            matrices.add(matrix);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return matrices;
    }

    private void emptyCurrentRow() {
        matrixRow.clear();
    }

    private void populateMatrixRowFromLine(String line) {
        String[] matrixSingleMembers = line.split(" ");

        for (String matrixSingleMember : matrixSingleMembers) {
            matrixRow.add(Double.parseDouble(matrixSingleMember));
        }
    }

    private void emptyMatrixRows() {
        matrixRows.clear();
    }

    void writeMatricesToFile(String outputFile, List<Matrix> matrices) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {

            for (Matrix matrix : matrices) {
                writer.write(matrix.getDeterminant() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
